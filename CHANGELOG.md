# ckanext-lcrnz changelog

## 2022-09-22 Version 2.8.21

- Added a version field for resources.

## 2022-09-22 Version 2.8.20

- Added max width to CSS to allow image embedding in description.

## 2022-09-16 Version 2.8.19

- Added a new field for overriding the spatial search parameters.
- Fixed lat/long to extent computation errors.
- Added a helper box to describe the lat/lon position (which is the center of the extent).
- Removed feed code as it was producing an error due to copied old code.
  Feed does not work and requires additional attention.

## 2022-09-12 Version 2.8.18

- Created a warning box on the registration page to inform company staff that they do not need to register.

## 2022-09-08 Version 2.8.17

- Fixed User Search and pagination bug.

## 2022-08-23 Version 2.8.16

- Added author help text.
- Updated doi placeholder text.
- Fixed license help text to use CC.

## 2022-08-23 Version 2.8.15

- Fixed the tag field.

## 2022-04-05 Version 2.8.13 & 2.8.14

- Fixed an error with showing default metadata preventing new data from being added.
- Changed the default IP address due to changes in virtualbox having a min ip range.

## 2022-03-03 Version 2.8.12

- Added additional missing fields from previous version.
- Removed several dataset fields from being displayed.
- Changed the display for maintainer and maintainer_email combining the 2 fields.
- Changed display property for source field.
- Added hiding empty fields in the dataset display page.  Note, this requires lcrnz to be added before composite and scheming.
- Changed custom fields to use composite and replaced composite_repeating display snippet.

## 2021-11-15 Version 2.8.11

- Reverting back in order to preserve custom fields.

## 2021-11-10 Version 2.8.10

- Updated lat/lon adding 2 additional fields for width and height.
- Changed the user defined fields to use composite instead of repeated text fields.

## 2021-09-17 Version 2.8.9

- Added lat/lon coordiantes similar to QUAVONZ.
- Updated validators.

## 2021-09-17 Version 2.8.8

- Updated the login and registration page.
- Changed programatic scheming in plugin to the ckanext-scheming method.
