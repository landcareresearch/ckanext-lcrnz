#!/bin/bash

function do_requirements {
  main_dir=$1

  if [ -f "${main_dir}/requirements-py2.txt" ] ; then
    install_file='drequirements-py2.txt'
  elif [ -f "${main_dir}/dev-requirements.txt" ] ; then
    install_file='dev-requirements.txt'
  elif [ -f "${main_dir}/requirements.txt" ] ; then
    install_file='requirements.txt'
  elif [ -f "${main_dir}/test-requirements.txt" ] ; then
    install_file='test-requirements.txt'
  else
    install_file=false
  fi

  if [ $install_file != false ] ; then
    pushd .
    cd ${main_dir}
    sudo ckan_activate_exec.bash pip install -r $install_file
    popd
  fi


}

function setup {
  pushd .
  sudo ckan_activate_exec.bash pip install $i
  cd $i
  sudo ckan_activate_exec.bash python setup.py install
  sudo ckan_activate_exec.bash python setup.py develop
  popd
}

dirs=$(ls -d ckanext*)
for i in $dirs ; do
  echo $i
  #do_requirements $i
  setup $i
done
