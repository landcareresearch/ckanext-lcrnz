module MyVars
    HOSTNAME               = "dev-datastore"
    OS_FILE                = "ubuntu18.04"
    MEMORY                 = "4096"
    CPUS                   = "1"
    IP                     = "192.168.56.24"
    GENCACHE               = false
    PROJECT_PATH           = "../"
    FOLDERS                = {
        "ckanext-lcrnz" => {
            "src"  => "../.",
            "dest" => "/opt/ckanext-lcrnz"
        }
    }
end
